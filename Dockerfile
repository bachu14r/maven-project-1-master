FROM openjdk:8-jre-alpine3.9

LABEL maintainer="bachu.14r@gmail.com"

ENV APPLICATION_PATH=/app1.jar

# copy the packaged jar file into our docker image
COPY server/target/server.jar ${APPLICATION_PATH}
 
# set the startup command to execute the jar
CMD ["java", "-jar", "/test.jar"]
